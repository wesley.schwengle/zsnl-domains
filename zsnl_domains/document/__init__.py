# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ..case_management.repositories import CaseRepository
from .commands import Commands
from .infrastructures import (
    ConverterInfrastructure,
    WopiInfrastructure,
    ZohoInfrastructure,
)
from .queries import Queries
from .repositories import (
    DirectoryEntryRepository,
    DirectoryRepository,
    DocumentLabelRepository,
    DocumentRepository,
    FileRepository,
    MessageRepository,
)
from minty_infra_misc import RedisInfrastructure

REQUIRED_REPOSITORIES = {
    "case": CaseRepository,
    "directory_entry": DirectoryEntryRepository,
    "directory": DirectoryRepository,
    "document_label": DocumentLabelRepository,
    "document": DocumentRepository,
    "file": FileRepository,
    "message": MessageRepository,
}

REQUIRED_INFRASTRUCTURE = {
    "converter": ConverterInfrastructure(),
    "redis": RedisInfrastructure(),
    "wopi": WopiInfrastructure(),
    "zoho": ZohoInfrastructure(),
}


def get_query_instance(repository_factory, context, user_uuid):
    return Queries(
        repository_factory=repository_factory,
        context=context,
        user_uuid=user_uuid,
    )


def get_command_instance(
    repository_factory, context, user_uuid, event_service
):
    return Commands(
        repository_factory=repository_factory,
        context=context,
        user_uuid=user_uuid,
        event_service=event_service,
    )
