# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

__version__ = "0.1.70"

from sqlalchemy import engine_from_config
from sqlalchemy.engine.base import Engine


def database_engine(config: dict) -> Engine:
    """Create a new SQLalchemy Engine from Zaaksysteem configuration

    :param config: Configuration for Zaaksysteem, as supplied by
                   minty_infrastructure
    :type config: dict
    :return: SQLAlchemy engine, ready to connect to the Zaaksysteem database
             for the current instance.
    :rtype: Engine
    """
    connect_info = config["ZaaksysteemDB"]["connect_info"]

    engine = engine_from_config(connect_info)
    return engine
