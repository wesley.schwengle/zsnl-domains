# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from enum import Enum
from minty import entity
from pydantic import Field
from typing import List, Optional, Union
from uuid import UUID
from zsnl_domains.shared.custom_field.types import base as typebase


class RelationshipTypeDocumentMetadata(entity.ValueObject):
    filename: str = Field(
        ..., title="The filename of this document including its extension"
    )


class RelationshipTypeMetadata(entity.ValueObject):
    summary: str = Field(..., title="Title of the custom object")
    description: Optional[str] = Field(
        None, title="Subtitle of the custom object"
    )


class RelationshipTypes(str, Enum):
    case = "case"
    custom_object = "custom_object"
    document = "document"
    subject = "subject"


class RelationshipCustomFieldDetails(entity.ValueObject):
    relationship_type: RelationshipTypes = Field(
        ...,
        title="Type of this relationship, one of [document,case,custom_object]",
    )
    metadata: Optional[
        Union[RelationshipTypeDocumentMetadata, RelationshipTypeMetadata]
    ] = Field(None, title="Optional metadata about the field")


class CustomFieldTypeRelationship(typebase.CustomFieldValueBase):
    type: str = Field(
        "relationship", title="A value referencing another type of object"
    )
    # value is of the type Union[UUID, "List[CustomFieldTypeRelationship]"].
    # It may contain:
    # - a direct relationship ({"value"="uuid"})
    # - an list of relationship data {"value"=[{"value" = "uuid",
    #    "specifics" : "something"}]}
    # Both are supported
    value: Union[UUID, "List[CustomFieldTypeRelationship]"] = Field(
        ...,
        title="A relation, or a list of relations to another field",
    )

    specifics: Optional[RelationshipCustomFieldDetails] = Field(
        None, title="Optional metadata about the field"
    )


CustomFieldTypeRelationship.update_forward_refs()
