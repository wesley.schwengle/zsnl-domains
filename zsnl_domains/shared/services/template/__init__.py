# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .template import TemplateService

__all__ = ["TemplateService"]
