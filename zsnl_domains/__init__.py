# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

__version__ = "0.6.247"

import minty.infrastructure
import operator
from .shared.types import ComparisonFilterCondition
from minty.repository import RepositoryBase
from minty_infra_amqp import AMQPInfrastructure
from minty_infra_sqlalchemy import DatabaseSessionInfrastructure
from sqlalchemy import sql
from typing import List


class DatabaseRepositoryBase(RepositoryBase):
    REQUIRED_INFRASTRUCTURE = {"amqp": AMQPInfrastructure()}
    REQUIRED_INFRASTRUCTURE_RW = {
        "database": DatabaseSessionInfrastructure("zaaksysteemdb.")
    }
    REQUIRED_INFRASTRUCTURE_RO = {
        "database": DatabaseSessionInfrastructure("zaaksysteemdb_ro.")
    }

    MAX_QUERYTIME_COUNT_MS = 100

    def __init__(
        self,
        infrastructure_factory: minty.infrastructure.InfrastructureFactory,
        context: str,
        event_service,
    ):
        super().__init__(
            infrastructure_factory=infrastructure_factory,
            context=context,
            event_service=event_service,
        )

        self.session = self.infrastructure_factory.get_infrastructure(
            context=self.context, infrastructure_name="database"
        )

    def apply_comparison_filter(
        self,
        query: sql.expression.Select,
        column,
        comparison_filters: List[ComparisonFilterCondition],
    ) -> sql.expression.Select:
        """
        Apply a list of `ComparisonFilterCondition` conditions to a
        SQLAlchemy query.

        Adds `WHERE column operator operand` for every filter in the list.

        Example:

        ```
        query = sql.select(schema.SomeTable)
        query = self.apply_comparison_filter(
            query=query,
            column=schema.SomeTable.id,
            comparison_filters=[
                ComparisonFilterCondition[int].new_from_str("gt 10"),
                ComparisonFilterCondition[int].new_from_str("le 100")
            ]
        )
        ```

        Returns a new query with the added conditions.
        """

        for filter in comparison_filters:
            query = query.where(
                getattr(operator, filter.operator)(column, filter.operand)
            )

        return query

    def _get_count(self, query: sql.expression.Select) -> int:
        """Get the total number of results for this query."""
        count_query = (
            query.with_only_columns([sql.func.count(sql.literal(1))])
            .limit(None)
            .offset(None)
            .order_by(None)
        )

        return self.session.execute(count_query).scalar()

    def _calculate_offset(self, page: int, page_size: int):
        """Given a page number and page size, calculate the offset at which it
        starts"""
        return (page * page_size) - page_size
