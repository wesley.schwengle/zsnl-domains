# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from datetime import datetime
from minty.cqrs.events import event
from minty.entity import EntityBase
from uuid import UUID


class DocumentTemplate(EntityBase):
    """Data structure which holds detailed information about an document template."""

    @property
    def entity_id(self):
        return self.uuid

    def __init__(
        self,
        uuid: UUID,
        id: int,
        name: str,
        file_uuid: UUID,
        file_name: str,
        integration_uuid: UUID,
        integration_reference: str,
        help: str,
        category_uuid: UUID,
    ):
        self.uuid = uuid
        self.id = id
        self.name = name
        self.file_uuid = file_uuid
        self.file_name = file_name
        self.integration_uuid = integration_uuid
        self.integration_reference = integration_reference
        self.category_uuid = category_uuid
        self.help = help
        self.commit_message = None
        self.deleted = None

    @event("DocumentTemplateCreated")
    def create(self, fields: dict):
        """Create a document template.

        :param fields: fields to be created
        :type fields: dict
        """
        allowed_fields = [
            "name",
            "file_uuid",
            "integration_uuid",
            "integration_reference",
            "category_uuid",
            "help",
            "commit_message",
        ]

        for key, value in fields.items():
            if key in allowed_fields:
                setattr(self, key, value)

    @event("DocumentTemplateEdited")
    def edit(self, fields: dict):
        """Update/edit document_template.

        :param fields: fields and fields to be updated
        :type fields: dict
        """
        allowed_fields = [
            "name",
            "file_uuid",
            "integration_reference",
            "category_uuid",
            "help",
            "commit_message",
        ]

        for key, value in fields.items():
            if key in allowed_fields:
                setattr(self, key, value)

    @event("DocumentTemplateDeleted")
    def delete(self, reason: str):
        """Delete an email_template.

        :param reason: reason for delete
        :type reason: str
        """

        self.deleted = datetime.now()
        self.commit_message = reason
