# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.entity import Entity
from pydantic import Field
from typing import Optional


class AttributeSearch(Entity):
    """Content of a attribute"""

    entity_type = "attribute"

    label: Optional[str] = Field(..., title="Label for the attribute")
    magic_string: Optional[str] = Field(
        ..., title="Magic string for the attribute"
    )
    description: Optional[str] = Field(
        ..., title="Description for the attribute"
    )
    type: Optional[str] = Field(..., title="Type for the attribute")
