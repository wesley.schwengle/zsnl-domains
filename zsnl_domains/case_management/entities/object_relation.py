# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.entity import Entity, Field
from typing import Optional
from uuid import UUID


class ObjectRelation(Entity):
    """Entity represents object relation of attribute in the case"""

    entity_type = "object_relation"
    entity_id__fields = ["uuid"]

    attribute_id: Optional[str] = Field(..., title="Id of the the attribute")
    source_custom_field_type_id: Optional[UUID] = Field(
        ..., title="Id of the the attribute"
    )
    magic_string: str = Field(..., title="Magic string of the attribute")
    uuid: Optional[UUID] = Field(
        ..., title="UUID of the custom object relationship"
    )
    custom_object_uuid: Optional[UUID] = Field(
        ..., title="UUID of the custom object"
    )
