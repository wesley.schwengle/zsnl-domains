# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
from typing import Optional
from uuid import UUID, uuid4
from zsnl_domains.case_management import entities

# Goal of this test file is to provide functions to create entities containing
# default values so they can be easily used within unittests.


def create_case(event_service, uuid: Optional[UUID]) -> entities.Case:
    return entities.Case(
        id=123,
        number=123,
        uuid=uuid4() if not uuid else uuid,
        entity_id=uuid4() if not uuid else uuid,
        department=entities.Department(
            uuid=uuid4(),
            name="Department of Mysteries",
            description="Who knows!",
        ),
        role=entities.Role(uuid=uuid4(), name="Administrator"),
        destruction_date=datetime.date(2019, 1, 1),
        archival_state="2019-23-23",
        status="open",
        created_date=datetime.date(2019, 10, 22),
        registration_date=datetime.date(2019, 10, 23),
        target_completion_date=datetime.date(2019, 10, 24),
        completion_date=datetime.date(2019, 10, 25),
        stalled_until_date=datetime.date(2019, 10, 22),
        milestone=1,
        extension_reason="no reason",
        suspension_reason="no reason",
        related_to={},
        completion="yes",
        stalled_since_date=datetime.date(2019, 12, 22),
        last_modified_date=datetime.date(2019, 4, 19),
        coordinator=None,
        assignee=None,
        _event_service=event_service,
    )
