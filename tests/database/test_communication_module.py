# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from zsnl_domains.database.schema import (
    Thread,
    ThreadMessage,
    ThreadMessageAttachment,
    ThreadMessageAttachmentDerivative,
    ThreadMessageContactmoment,
    ThreadMessageExternal,
    ThreadMessageNote,
)


class TestSchemaCommunicationModule:
    """Test database schema"""

    def test_thread(self):
        fields = [
            "id",
            "uuid",
            "contact_uuid",
            "contact_displayname",
            "case_id",
            "created",
            "last_modified",
            "thread_type",
            "last_message_cache",
            "message_count",
            "unread_employee_count",
            "unread_pip_count",
            "attachment_count",
        ]

        for field in fields:
            assert hasattr(Thread, field)

    def test_thread_message(self):
        fields = [
            "id",
            "uuid",
            "thread_id",
            "type",
            "message_slug",
            "created_by_uuid",
            "created_by_displayname",
            "created",
            "last_modified",
            "thread_message_note_id",
            "thread_message_contact_moment_id",
        ]

        for field in fields:
            assert hasattr(ThreadMessage, field)

    def test_thread_message_note(self):
        fields = ["id", "content"]

        for field in fields:
            assert hasattr(ThreadMessageNote, field)

    def test_thread_message_contact_moment(self):
        fields = [
            "id",
            "content",
            "contact_channel",
            "direction",
            "recipient_uuid",
            "recipient_displayname",
        ]

        for field in fields:
            assert hasattr(ThreadMessageContactmoment, field)

    def test_thread_message_external(self):
        fields = [
            "id",
            "content",
            "subject",
            "type",
            "participants",
            "direction",
            "source_file_id",
            "read_employee",
            "read_pip",
            "attachment_count",
            "failure_reason",
        ]

        for field in fields:
            assert hasattr(ThreadMessageExternal, field)

    def test_thread_message_attachment(self):
        fields = [
            "uuid",
            "id",
            "filestore_id",
            "filename",
            "thread_message_id",
        ]

        for field in fields:
            assert hasattr(ThreadMessageAttachment, field)

    def test_thread_message_attachment_derivative(self):
        fields = [
            "id",
            "thread_message_attachment_id",
            "filestore_id",
            "max_width",
            "max_height",
            "date_generated",
            "type",
        ]

        for field in fields:
            assert hasattr(ThreadMessageAttachmentDerivative, field)
