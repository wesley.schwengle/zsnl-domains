# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
import uuid
from datetime import date, datetime, timedelta, timezone
from sqlalchemy.dialects.postgresql import UUID as pgUUID
from sqlalchemy.types import CHAR
from zsnl_domains.database.types import GUID, UTCDate, UTCDateTime


class TestTypes:
    """Test custom database types"""

    def _get_fake_dialect(self):
        class FakeDialect:
            name = "postgresql"

            def type_descriptor(self, descriptor):
                self.descriptor = descriptor
                return "XXX"

        return FakeDialect()

    def test_load_dialect_impl(self):
        g = GUID()

        fake_dialect = self._get_fake_dialect()

        rv = g.load_dialect_impl(fake_dialect)

        assert rv == "XXX"
        assert isinstance(fake_dialect.descriptor, pgUUID)

        fake_dialect.name = "theirsql"
        rv = g.load_dialect_impl(fake_dialect)

        assert rv == "XXX"
        assert isinstance(fake_dialect.descriptor, CHAR)
        assert not isinstance(fake_dialect.descriptor, pgUUID)

        assert fake_dialect.descriptor.length == 32

    def test_GUID_process_bind_param(self):
        g = GUID()

        fake_dialect = self._get_fake_dialect()

        assert g.process_bind_param(None, fake_dialect) is None

        # PostgreSQL dialect: use UUID type
        assert (
            g.process_bind_param(
                uuid.UUID("3eb921de-b6d0-4fce-9565-785c6017fd95"), fake_dialect
            )
            == "3eb921de-b6d0-4fce-9565-785c6017fd95"
        )

        # Other dialects: use 32 byte hex strings
        fake_dialect.name = "theirsql"

        assert (
            g.process_bind_param(
                "3eb921de-b6d0-4fce-9565-785c6017fd95", fake_dialect
            )
            == "3eb921deb6d04fce9565785c6017fd95"
        )
        assert (
            g.process_bind_param(
                uuid.UUID("3eb921de-b6d0-4fce-9565-785c6017fd95"), fake_dialect
            )
            == "3eb921deb6d04fce9565785c6017fd95"
        )

    def test_GUID_process_result_value(self):
        g = GUID()

        assert g.process_result_value(None, self._get_fake_dialect()) is None
        assert g.process_result_value(
            uuid.UUID("3eb921de-b6d0-4fce-9565-785c6017fd95"),
            self._get_fake_dialect(),
        ) == uuid.UUID("3eb921de-b6d0-4fce-9565-785c6017fd95")
        assert g.process_result_value(
            "3eb921de-b6d0-4fce-9565-785c6017fd95", self._get_fake_dialect()
        ) == uuid.UUID("3eb921de-b6d0-4fce-9565-785c6017fd95")

    def test_UTCDateTime(self):
        dt = UTCDateTime()

        rv = dt.process_result_value(None, None)
        assert rv is None

        timestamp = datetime(1955, 10, 12, 22, 4, 11)
        assert timestamp.tzinfo is None
        rv = dt.process_result_value(timestamp, None)

        assert rv.year == 1955
        assert rv.month == 10
        assert rv.day == 12
        assert rv.hour == 22
        assert rv.minute == 4
        assert rv.second == 11
        assert rv.tzinfo == timezone.utc

    def test_UTCDateTime_save_none(self):
        dt = UTCDateTime()

        rv = dt.process_bind_param(None, None)
        assert rv is None

    def test_UTCDateTime_save_localtime(self):
        dt = UTCDateTime()
        tz = timezone(timedelta(hours=2))

        rv = dt.process_bind_param(None, None)
        assert rv is None

        timestamp = datetime.now(tz)
        assert timestamp.tzinfo == tz
        rv = dt.process_bind_param(timestamp, None)

        assert rv == timestamp.astimezone(timezone.utc)

    def test_UTCDateTime_save_invalid_value(self):
        dt = UTCDateTime()

        # By default, datetime.now does not include a time zone.
        # Use datetime.datetime.utcnow(datetime.timezone.utc)
        timestamp = datetime.now()

        with pytest.raises(ValueError):
            dt.process_bind_param(timestamp, None)

    def test_UTCDateTime_save_not_datetime(self):
        dt = UTCDateTime()

        not_timestamp = date.today()
        with pytest.raises(TypeError):
            dt.process_bind_param(not_timestamp, None)

    def test_UTCDateTime_incorrect_string(self):
        dt = UTCDateTime()

        not_timestamp = "foo"
        with pytest.raises(ValueError):
            dt.process_bind_param(not_timestamp, None)

    def test_UTCDate_process_bind_params(self):
        dt = UTCDate()
        assert dt.process_bind_param(None, None) is None

        input_date = "2019-07-19"
        expected = datetime(
            year=2019, month=7, day=19, hour=0, minute=0, second=0
        )
        assert (
            dt.process_bind_param(value=input_date, dialect=None) == expected
        )

        input_date2 = date(year=2019, month=7, day=19)
        expected2 = datetime(
            year=2019, month=7, day=19, hour=0, minute=0, second=0
        )
        assert (
            dt.process_bind_param(value=input_date2, dialect=None) == expected2
        )

    def test_UTCDate_result_value(self):
        dt = UTCDate()
        assert dt.process_result_value(None, None) is None
        input_date = datetime(
            year=2019, month=7, day=19, hour=0, minute=0, second=0
        )
        expected = date(year=2019, month=7, day=19)
        assert (
            dt.process_result_value(value=input_date, dialect=None) == expected
        )

        input_date2 = datetime(
            year=2019, month=7, day=19, hour=23, minute=23, second=12
        )
        expected2 = date(year=2019, month=7, day=20)
        assert (
            dt.process_result_value(value=input_date2, dialect=None)
            == expected2
        )
